
#include <msp430.h>				

int main(void) {
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer
	P1DIR |= 0x01;					// Set P1.0 to output direction
	volatile unsigned int i;	// volatile to prevent optimization
		unsigned int tempo= 10000;

	for(;;) {

		P1OUT ^= 0x01;				// Toggle P1.0 using exclusive-OR

		i = tempo;					// SW Delay
		do i--;
		while(i != 0);
	}
	
	return 0;
}
